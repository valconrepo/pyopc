from PyOv import PyOv
from PyOv import OvationDataItem
import sys
import configparser
import logging


sys.path.insert(0, "..")
import time
import csv

from opcua import ua, Server


class OPCObject:
    def __init__(self, OvData):
        self.OvData = OvData
        self.OPCDataObject = objects.add_object(idx, OvData.pointName)
        self.OPCVarValue = self.OPCDataObject.add_variable(idx, "Value", OvData.value)
        self.OPCVarTime = self.OPCDataObject.add_variable(idx, "Time", OvData.time)
        self.OPCVarSID = self.OPCDataObject.add_variable(idx, "sid", OvData.sid)
        self.OPCVarStatus = self.OPCDataObject.add_variable(idx, "status", OvData.status)
        self.OPCVarValue.set_writable()

    def read(self, OvationGateway):
        OvationGateway.GetValueAdv(self.OvData)
        self.OPCVarValue.set_value(self.OvData.value)
        self.OPCVarTime.set_value(self.OvData.time)
        self.OPCVarStatus.set_value(self.OvData.status)

    def write(self, OvationGateway):
        self.OvData.value = self.OPCVarValue.get_value()
        if self.OvData.value is None:
            self.OvData.value = 0
        OvationGateway.PutValueAdv(self.OvData)


if __name__ == "__main__":

    #set up loggers
    logging.basicConfig(filename='PyOPC.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s', level = logging.WARNING)
    OPClogger = logging.getLogger("opcua")
    OPClogger.setLevel(logging.WARNING)

    # Open and read config file
    config = configparser.ConfigParser()
    try:
        with open('config.cnf', 'r') as configfile:
            config.read_file(configfile)
            Endpoint = config ['DEFAULT'] ['Endpoint']
            ScanRate = float(config ['DEFAULT'] ['ScanRate'])
    except:
        logging.exception("Unable to parse config.cnf file!")
        sys.exit()



    # Initialise Ovation gateway
    OvationGateway = PyOv()
    OvationGateway.OvationConnect()
    if not OvationGateway.connected:
        logging.exception("Unable to connect to Ovation!")
        sys.exit()

    # Open and read variables file
    try:
        with open('variables.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',', dialect='excel')
            OvationVariableListRead = []
            OvationVariableListWrite = []
            for row in csv_reader:
                OvData = OvationDataItem(pointName=row [1])
                if row [2] == "LA":
                    OvData.type = 91
                if row [2] == "LD":
                    OvData.type = 141
                OvationGateway.GetValueAdv(OvData)
                if row [3] == "R":
                    OvationVariableListRead.append(OvData)
                if row [3] == "W":
                    OvationVariableListWrite.append(OvData)
    except:
        logging.exception("Unable to parse variables.csv file!")
        sys.exit()


    # setup our server
    server = Server()
    server.set_endpoint(Endpoint)

    # setup our own namespace, not really necessary but should as spec
    uri = "http://examples.freeopcua.github.io"
    idx = server.register_namespace(uri)

    # get Objects node, this is where we should put our nodes
    objects = server.get_objects_node()

    # Initiate OPC objects for each variable
    OvObjListRead = []
    OvObjListWrite = []

    for OvData in OvationVariableListRead:
        OvObjListRead.append(OPCObject(OvData))
    for OvData in OvationVariableListWrite:
        OvObjListWrite.append(OPCObject(OvData))


    try:
        # starting!
        server.start()
    except:
        logging.exception("OPC server cannot be started!")
    else:
        logging.info("OPC server started!")
        # update variables every scan rate
        try:
            while True:
                time.sleep(ScanRate / 1000)
                for OvObj in OvObjListRead:
                    OvObj.read(OvationGateway)
                for OvObj in OvObjListWrite:
                    OvObj.write(OvationGateway)
                logging.debug("Updated all objects!")

        finally:
            # close connection, remove subcsriptions, etc
            server.stop()
            logging.info("Server stopped!")

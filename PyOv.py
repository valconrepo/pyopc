from ctypes import *
import logging
import os
from win32api import LoadLibraryEx
from win32con import LOAD_WITH_ALTERED_SEARCH_PATH


class OvationDataItem:
    def __init__(self,  **kwargs):
        self.value = kwargs.get("value", None)
        self.status = kwargs.get("status", None)
        self.time = kwargs.get("time", None)
        self.pointName = kwargs.get("pointName", None)
        self.sid = kwargs.get("sid", None)
        self.type = kwargs.get("type", None)
        self.readwrite = kwargs.get("type", None)

    def __dir__(self):
        return ['value', 'status', 'time', 'pointName', 'sid', 'type']

    def merge(self, OvData):
        attributes_list = dir(OvData)
        for attribute in attributes_list:
            attribute_value = getattr(OvData, attribute)
            if attribute_value is not None:
                setattr(self, attribute, attribute_value)


class PyOv:
    def __init__(self):
        logPath = os.getcwd()
        fileName = "PyOv.log"

        logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
        rootLogger = logging.getLogger()
        rootLogger.setLevel(logging.DEBUG)

        fileHandler = logging.FileHandler("{0}/{1}.log".format(logPath, fileName))
        fileHandler.setFormatter(logFormatter)
        rootLogger.addHandler(fileHandler)

        consoleHandler = logging.StreamHandler()
        consoleHandler.setFormatter(logFormatter)
        rootLogger.addHandler(consoleHandler)

        self.connected = False
        self.wdpf_spd = -1

        dll_name = "C:\Ovation\OvationBase\OvationDataAPIWrapper.dll"
        dll_handle = LoadLibraryEx(dll_name, 0, LOAD_WITH_ALTERED_SEARCH_PATH)
        dll = cdll.LoadLibrary(dll_name)
        self.dll = dll

        c_connect = getattr(dll, "?Connect@@YAJPAH@Z")
        c_connect.argtypes = [POINTER(c_int)]
        c_connect.restype = c_long
        self.c_connect = c_connect

        c_getsid = getattr(dll, "?SPD_GetSid@@YAJHPADPAK@Z")
        c_getsid.argtypes = [c_int, c_char_p, POINTER(c_ulong)]
        self.c_getsid = c_getsid

        c_getanalogvalue = getattr(dll, "?SHC_getAnalogValue@@YAJKPAMPAGPAK@Z")
        c_getanalogvalue.argtypes = [c_uint, POINTER(c_float), POINTER(c_ushort), POINTER(c_uint)]
        self.c_getanalogvalue = c_getanalogvalue

        c_getdigitalvalue = getattr(dll, "?SHC_getDigitalValue@@YAJKPAG0PAK@Z")
        c_getdigitalvalue.argtypes = [c_uint, POINTER(c_ushort), POINTER(c_ushort), POINTER(c_uint)]
        self.c_getdigitalvalue = c_getdigitalvalue

        c_getpointtype = getattr(dll, "?SPD_GetPointType@@YAJHKPAE@Z")
        c_getpointtype.argtypes = [c_int, c_uint, c_char_p]
        self.c_getpointtype = c_getpointtype

        c_putanalogvalue = getattr(dll, "?SHC_putAnalogValue@@YAJKPAM@Z")
        c_putanalogvalue.argtypes = [c_ulong, POINTER(c_float)]
        self.c_putanalogvalue = c_putanalogvalue

        c_putdigitalvalue = getattr(dll, "?SHC_putDigitalValue@@YAJKPAK@Z")
        c_putdigitalvalue.argtypes = [c_ulong, POINTER(c_ulong)]
        self.c_putdigitalvalue = c_putdigitalvalue



    def OvationConnect(self):
        wdpf_spd = c_int(-1)
        err = self.c_connect(byref(wdpf_spd))
        if err != 0:
            self.connected = False
            logging.exception("Cannot connect to Ovation")
        else:
            self.connected = True
            self.wdpf_spd = wdpf_spd
            logging.info("Connected to Ovation")

    def GetPointSID(self, pointName):
        c_sid = c_uint(0)
        c_pointName = pointName.encode('utf-8')

        if not self.connected:
            self.OvationConnect()
        err = self.c_getsid(self.wdpf_spd, c_pointName, byref(c_sid))
        if err != 0:
            logging.exception("GetPointSID Error")
        return c_sid.value

    def GetAnalogValue(self, sid):
        c_value = c_float(0)
        c_status = c_ushort(0)
        c_time = c_uint(0)
        c_sid = c_uint(sid)

        if not self.connected:
            self.OvationConnect()
        err = self.c_getanalogvalue(c_sid, byref(c_value), byref(c_status), byref(c_time))
        if err != 0:
            logging.exception(f'Error on sid: {sid}')

        ovData = OvationDataItem(value=c_value.value, status=c_status.value, time=c_time.value)
        return ovData

    def GetDigitalValue(self, sid):
        c_value = c_ushort(0)
        c_status = c_ushort(0)
        c_time = c_uint(0)
        c_sid = c_uint(sid)

        if not self.connected:
            self.OvationConnect()

        err = self.c_getdigitalvalue(c_sid, byref(c_value), byref(c_status), byref(c_time))

        if err != 0:
            logging.exception(f'Error on sid: {sid}')
        ovData = OvationDataItem(value=c_value.value, status=c_status.value, time=c_time.value)
        return ovData

    def GetPointType(self, sid):
        blank_string = "0"
        c_type = c_char_p(0)
        c_sid = c_uint(sid)
        if not self.connected:
            self.OvationConnect()
        err = self.c_getpointtype(self.wdpf_spd, c_sid, c_type)
        if not err == 0:
            logging.exception("GetPointType Error")
        return c_type.value

    def GetValuebySID(self, sid):
        py_type = self.GetPointType(sid)
        switcher = {
            141: self.GetDigitalValue,
            91: self.GetAnalogValue
        }
        func = switcher.get(py_type, lambda sid: 'Invalid point type')
        value = func(sid)
        return value

    def GetValuebyName(self, pointName):
        sid = self.GetPointSID(pointName)
        py_type = self.GetPointType(sid)
        switcher = {
            141: self.GetDigitalValue,
            91: self.GetAnalogValue
        }
        func = switcher.get(py_type, lambda sid: 'Invalid point type')
        value = func(sid)
        return value

    def GetValueAdv(self, OvData):
        if OvData.sid is None:
            OvData.sid = self.GetPointSID(OvData.pointName)

        if OvData.type == 91:
            Data = self.GetAnalogValue(OvData.sid)
            OvData.merge(Data)
        if OvData.type == 141:
            Data = self.GetDigitalValue(OvData.sid)
            OvData.merge(Data)


    def PutAnalogValue(self, sid, value):
        c_value = c_float(value)
        c_sid = c_uint(sid)

        if not self.connected:
            self.OvationConnect()
        err = self.c_putanalogvalue(c_sid, byref(c_value))
        if err != 0:
            logging.exception(f'Error on sid: {sid}, error {err}')

    def PutDigitalValue(self, sid, value):
        c_value = c_ulong(value)
        c_sid = c_uint(sid)

        if not self.connected:
            self.OvationConnect()
        err = self.c_putdigitalvalue(c_sid, byref(c_value))
        if err != 0:
            logging.exception(f'Error on sid: {sid}, Error {err}')

    def PutValueAdv(self,  OvData):
        if OvData.sid is None:
            OvData.sid = self.GetPointSID(OvData.pointName)
        if OvData.type == 91:
            self.PutAnalogValue(OvData.sid, OvData.value)
        if OvData.type == 141:
            self.PutDigitalValue(OvData.sid, OvData.value)



if __name__ == '__main__':
    OvationGateway = PyOv()
    AnalogPoint = "TEST_ANALOG.UNIT0@NET0"
    sidAnalog = OvationGateway.GetPointSID(AnalogPoint)
    """
    print(f"SID of point {AnalogPoint} is {sidAnalog}")
    OvationDataAnalog = OvationGateway.GetAnalogValue(sidAnalog)
    print(f"Value of point {AnalogPoint} is {OvationDataAnalog.value}")
    print(f"Status of point {AnalogPoint} is {OvationDataAnalog.status}")
    print(f"Timestamp of point {AnalogPoint} is {OvationDataAnalog.time}")
    DigitalPoint = "TYPICAL_HIC2XB06.UNIT0@NET0"
    sidDigital = OvationGateway.GetPointSID(DigitalPoint)
    print(f"SID of point {DigitalPoint} is {sidDigital}")
    OvationDataDigital = OvationGateway.GetDigitalValue(sidDigital)
    print(f"Value of point {DigitalPoint} is {OvationDataDigital.value}")
    print(f"Status of point {DigitalPoint} is {OvationDataDigital.status}")
    print(f"Timestamp of point {DigitalPoint} is {OvationDataDigital.time}")
    """
    OvationGateway.PutAnalogValue(sidAnalog, 6)

